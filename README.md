This is the tornado repo or the Ridealong driver web app.

To get started do the following on your command prompt or terminal:

1. Create the folder where you would like to have your local repo.
2. Type "git init" to initialize the folder as a local git repo.
3. Copy the link at the top right of this page and type "git remote add origin <copied-link>".
4. Type "git pull origin develop".
5. Create a branch to do your work with the following command "git branch <your-new-branch>".
6. Checkout the new branch like so "git checkout <your-new-branch>".
7. Open Netbeans and open the project from where it is located.

You are then set to do awesome work!

contact bulama@intellectualapps.com for any further guidelines.